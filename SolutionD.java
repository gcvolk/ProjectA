package com.xpwork.romantonum;


/*
 * 
 * Possible usercase according condition.
Roman   Arabic   Repeat criteria    Subtractable
I         1      3                     Yes
V         5      0                     No
X        10      3                     Yes
L        50      0                     No
C       100      3                     Yes
D       500      0                     No
M      1000      3                     No

 */

/**
 * @author
 *
 */
public class NumberToRomanDry{

	/**
	 * This function is convert integer to Roman 
	 * @param n would be in range 1 to 3999 
	 * @return 
	 * 
	 */
	public static String IntegerToRoman(int n){
		String roman="";
		int repeat;
		int magnitude[]={1000,900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
		String symbol[]={"M","CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
		repeat=n/1;
		for(int x=0; n>0; x++){
			repeat=n/magnitude[x];
			for(int i=1; i<=repeat; i++){
				roman=roman + symbol[x];
			}
			n=n%magnitude[x];
		}
		return roman;
	}

	/**
	 * @param args
	 */
	public static void main(String args[]){
		System.out.println("64: "+IntegerToRoman(64)); 
		System.out.println("226: "+IntegerToRoman(226)); 
		System.out.println("900: "+IntegerToRoman(900)); 
		System.out.println("998: "+IntegerToRoman(998)); 
		System.out.println("1712: "+IntegerToRoman(1712)); 

		System.out.println("1234: "+IntegerToRoman(1234)); 
	}
}
