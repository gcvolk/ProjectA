
/*
Created by Gil
*/

public class RomanConvertion {
	
	public static void main(String[] args) {
		System.out.println("64 Roman is " + getRoman(64));
	}
	
	public static  String getRoman(int number) {
		
		// Stores the standard roman numeral Strings 
	    String roman[] = {"M","XM","CM","D","XD","CD","C","XC","L","XL","X","IX","V","IV","I"};
	    //create a corresponding array with respective decimal values as in roman
	    int arab[] = {1000, 990, 900, 500, 490, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	    StringBuilder result = new StringBuilder();
	    
	    // parse through the number to be converted to convert the number to roman numeral
	    int i = 0;
	    while (number > 0 || arab.length == (i - 1)) {
	        while ((number - arab[i]) >= 0) {
	            number -= arab[i];
	            result.append(roman[i]);
	        }
	        i++;
	    }
	    return result.toString();
	}
	
	
}
